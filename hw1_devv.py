"""M3C 2018 Homework 1  Aleksander Grzyb 01072956
"""
import numpy as np
import matplotlib.pyplot as plt


def simulate1(N,Nt,b,e):
    """Simulate C vs. M competition on N x N grid over
    Nt generations. b and e are model parameters
    to be used in fitness calculations
    Output: S: Status of each gridpoint at tend of somulation, 0=M, 1=C
    fc: fraction of villages which are C at all Nt+1 times
    Do not modify input or return statement without instructor's permission.
    """
    #Set initial condition
    S  = np.ones((N,N),dtype=int) #Status of each gridpoint: 0=M, 1=C
    j = int((N-1)/2)
    S[j-1:j+2,j-1:j+2] = 0

    fc = np.zeros(Nt+1) #Fraction of points which are C
    fc[0] = S.sum()/(N*N)
    
    #plot_S(S)
    
    for k in range(Nt):
        "Calculate fitness score"
        score = np.zeros((N,N),dtype=float)
        for i in range(N):
            for j in range(N):
                neighbours=0
                if S[i][j]==1:  #check if C
                    if i-1>=0:          #check i-1 is a valid index
                        score[i][j]+=S[i-1][j]
                        neighbours+=1
                        if j-1>=0:          #check if also j-1 is a valid index
                            score[i][j]+=S[i-1][j-1]
                            neighbours+=1
                        if j+1<=N-1:        #check if also j+1 is a valid index
                            score[i][j]+=S[i-1][j+1]
                            neighbours+=1
                    if i+1<=N-1:        #check i+1 is a valid index
                        score[i][j]+=S[i+1][j]
                        neighbours+=1
                        if j-1>=0:          #check if also j-1 is a valid index
                            score[i][j]+=S[i+1][j-1]
                            neighbours+=1
                        if j+1<=N-1:        #check if also j+1 is a valid index
                            score[i][j]+=S[i+1][j+1]
                            neighbours+=1
                    if j-1>=0:          #check j-1 is a valid index
                        score[i][j]+=S[i][j-1]
                        neighbours+=1
                    if j+1<=N-1:        #check j+1 is a valid index
                        score[i][j]+=S[i][j+1]
                        neighbours+=1
                        
                if S[i][j]==0:  #check if M
                    if i-1>=0:          #check i-1 is a valid index
                        score[i][j]+=b*S[i-1][j]+e*int(S[i-1][j]==0)
                        neighbours+=1
                        if j-1>=0:          #check if also j-1 is a valid index
                            score[i][j]+=b*S[i-1][j-1]+e*int(S[i-1][j-1]==0)
                            neighbours+=1
                        if j+1<=N-1:        #check if also j+1 is a valid index
                            score[i][j]+=b*S[i-1][j+1]+e*int(S[i-1][j+1]==0)
                            neighbours+=1
                    if i+1<=N-1:        #check i+1 is a valid index
                        score[i][j]+=b*S[i+1][j]+e*int(S[i+1][j]==0)
                        neighbours+=1
                        if j-1>=0:          #check if also j-1 is a valid index
                            score[i][j]+=b*S[i+1][j-1]+e*int(S[i+1][j-1]==0)
                            neighbours+=1
                        if j+1<=N-1:        #check if also j+1 is a valid index
                            score[i][j]+=b*S[i+1][j+1]+e*int(S[i+1][j+1]==0)
                            neighbours+=1
                    if j-1>=0:          #check j-1 is a valid index
                        score[i][j]+=b*S[i][j-1]+e*int(S[i][j-1]==0)
                        neighbours+=1
                    if j+1<=N-1:        #check j+1 is a valid index
                        score[i][j]+=b*S[i][j+1]+e*int(S[i][j+1]==0)
                        neighbours+=1
                score[i][j]=score[i][j]/neighbours
                    
        "Probability matrix of being C next year"
        C_prob=np.zeros((N,N),dtype=float)
        for i in range(N):
                for j in range(N):
                    cfit=0              #initialize total fitness of C villages in the community
                    tfit=0              #initialize total fitness of all villages in the community
                    cfit+=S[i][j]*score[i][j] #a compact way to write an if statement
                    tfit+=score[i][j]
                    if i-1>=0:          #check i-1 is a valid index
                        cfit+=S[i-1][j]*score[i-1][j]
                        tfit+=score[i-1][j]
                        if j-1>=0:          #check if also j-1 is a valid index
                            cfit+=S[i-1][j-1]*score[i-1][j-1]
                            tfit+=score[i-1][j-1]
                        if j+1<=N-1:        #check if also j+1 is a valid index
                            cfit+=S[i-1][j+1]*score[i-1][j+1]
                            tfit+=score[i-1][j+1]
                    if i+1<=N-1:        #check i+1 is a valid index
                        cfit+=S[i+1][j]*score[i+1][j]
                        tfit+=score[i+1][j]
                        if j-1>=0:          #check if also j-1 is a valid index
                            cfit+=S[i+1][j-1]*score[i+1][j-1]
                            tfit+=score[i+1][j-1]
                        if j+1<=N-1:        #check if also j+1 is a valid index
                            cfit+=S[i+1][j+1]*score[i+1][j+1]
                            tfit+=score[i+1][j+1]
                    if j-1>=0:          #check j-1 is a valid index
                        cfit+=S[i][j-1]*score[i][j-1]
                        tfit+=score[i][j-1]
                    if j+1<=N-1:        #check j+1 is a valid index
                        cfit+=S[i][j+1]*score[i][j+1]
                        tfit+=score[i][j+1]
                    C_prob[i][j]=cfit/tfit
                    
        "Next generation"
        S=np.random.binomial(1,C_prob)
        fc[k+1]=S.sum()/(N*N)
        
        #plot_S(S)
        
    return S,fc

def plot_S(S):
    """Simple function to create plot from input S matrix
    """
    ind_s0 = np.where(S==0) #C locations
    ind_s1 = np.where(S==1) #M locations
    plt.plot(ind_s0[1],ind_s0[0],'rs')
    plt.hold(True)
    plt.plot(ind_s1[1],ind_s1[0],'bs')
    plt.hold(False)
    plt.show()
    plt.pause(0.05)
    return None


def simulate2(N,Nt,b,e):
    """Simulation code for Part 2, add input variables as needed
    """
    
def analyze(N,Nt,N_samples,npoints,bmin,bmax,e):
    """ Add input variables as needed
    """
    b_range=np.linspace(bmin,bmax,npoints)   #investigate npoints intermediate values of b
    Sum=np.zeros((npoints,Nt+1))
    for k in range(N_samples):         #Simulate N_samples times to estimate EV
        fc=np.zeros((npoints,Nt+1))
        for i in range(npoints):
            fc[i]=simulate1(N,Nt,b_range[i],e)[1]
        Sum+=fc
    Avg=Sum/N_samples
    plt.figure(figsize=(8, 6))
    plt.title('Aleksander Grzyb - analyze',fontsize=12)
    plt.xlabel('t',fontsize=14)
    plt.ylabel('fc(t)',fontsize=14)
    for i in range(npoints):          #plot average fc for different values of b
        plt.plot(Avg[i],label=('b = '+str(b_range[i])))
        plt.legend(fontsize='small')
        plt.hold(True)
        
    """In order to invesigate whether M becomes more successful with increasing values of b,
    we consider fc for different values of b. To minimize the effect of randomness, for
    each value of b, the process is simulated N_samples times and averaged. Respective
    averages are then plotted."""

if __name__ == '__main__':
    #The code here should call analyze and generate the
    #figures that you are submitting with your code
    
#Please note that first and third function takes a while to execute. 
#It may be advisable to run them one by one.
    
#    analyze(21,15,100,6,1,1.5,0.01)
    """An average of 100 samples for t<=15 clearly confirms the hypothesis that increasing
        values of b, corresponds to higher success rate of M-villages.
    """
    
#    analyze(21,200,1,6,1,1.5,0.01)
    """For large t, fc(t) eventually converges to 1 for small values of b, and to 0 for large.
    This is consistent with previous observation.
    """
    
#    analyze(21,300,10,11,1,1.5,0.01)
    """ The average for middle values of b converges to a constant fractional value,
    which indicates that for these values of b it is impossible to decide with probability ~1 
    at t=0, whether C-villages or M-vllages will dominate. This fractional value, gives us
    a rough estimate of the probability (at t=0) of the event: "All villages will become C eventually".
    The closer it is to 0.5, the more balanced the situation is for that value of b.
    """
#    output = analyze(21,15,1,4,15,75,0.01)
    """For very large values of b the shape of graph does not change. This is due to the fact
    that during each iteration only immediate neighbours of M-villages can become M-villages,
    i.e. for t=0,1,2,... the maximal number of M-villages is 9,16,25,... thus fc is bounded from below.
    """
